<?php get_header(); ?>

	<div id="page_404">
		<h1>404 NOT FOUND</h1>
		<p>お探しのページは見つかりません。移動したか、削除された可能性があります。</p>
	</div>

<?php get_footer(); ?>