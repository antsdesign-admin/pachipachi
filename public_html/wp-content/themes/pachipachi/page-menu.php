<?php 
/* Template Name: MENU */
get_header(); ?>
	<div id="menu_title" class="header_title">
		<h1><span class="font_en">Menu</span>メニュー</h1>
	</div>

	<div id="page_menu">
		<div class="clear">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
		</div>
		<div id="menu_navTop" class="menu_navigation">
			<ul class="menu_navigation_list">
				<li>
					<a href="#menu_viking">バイキング</a>
				</li>
<!--				<li>
					<a href="#menu_steak">ステーキ食べ放題</a>
				</li>-->
<!--				<li>
					<a href="#menu_lunch">ランチバイキング</a>
				</li>-->
<!--				<li>
					<a href="#menu_dinner">ディナーバイキング</a>
				</li>-->
<!--				<li>
					<a href="#menu_yakiniku">焼肉食べ放題</a>
				</li>-->
<!--				<li>
					<a href="#menu_cafe">カフェ</a>
				</li>-->
				<li>
					<a href="#menu_takeout">テイクアウト</a>
				</li>
				<?php /*
				<li>
					<a href="#menu_cafe">カフェ</a>
				</li>
*/?>
				<li>
					<a href="#menu_party">貸切パーティー</a>
				</li>
				<?php /*
				<li>
					<a href="#menu_party"><span class="font_en">PARTY</span>宴会</a>
				</li>*/?>
			</ul>
		</div>
		
		<?php /*?>
		<div id="menu_navBottom" class="menu_navigation">
			<ul id="nav_top" class="wrapper">
				<li>
					<a href="#menu_kids"><span class="font_en">Kid's</span>お子様用</a>	
				</li>
				<li>
					<a href="#menu_drink"><span class="font_en">Drink</span>ドリンク</a>
				</li>
				<li>
					<a href="#menu_appetizer"><span class="font_en">Appetizer</span>おつまみ</a>
				</li>
				<li>
					<a href="#menu_salad"><span class="font_en">Salad</span>サラダ</a>
				</li>
				<li>
					<a href="#menu_soup"><span class="font_en">Soup</span>スープ</a>
				</li>
				<li>
					<a href="#menu_meat"><span class="font_en">Meat</span>肉料理</a>
				</li>
				<li class="last">
					<a href="#menu_seafood"><span class="font_en">Sea food</span>シーフード</a>
				</li>
			</ul>
			<ul id="nav_bottom" class="wrapper">
				<li>
					<a href="#menu_pasta"><span class="font_en">Pasta</span>パスタ</a>
				</li>
				<li>
					<a href="#menu_pizza"><span class="font_en">Pizza</span>ピザ</a>
				</li>
				<li>
					<a href="#menu_rice"><span class="font_en">Rice</span>ライス</a>
				</li>
				<li>
					<a href="#menu_dessert"><span class="font_en">Dessert</span>デザート</a>
				</li>
				<li>
					<a href="#menu_course"><span class="font_en">Course</span>コース</a>
				</li>
				<li class="last">
					<a href="#menu_reservation"><span class="font_en">Reservation</span>要予約</a>
				</li>
			</ul>
		</div>*/?>
		
		<div id="menu_price" class="wrapper">
		
		<?php //if (is_user_logged_in()) { ?>
			<article id="menu_viking" class="menu_container photo-column-2">
				<?php
					$page_id = get_page_by_path( 'viking' );
					$page = get_post( $page_id );
										
					echo do_shortcode( $page->post_content );
				?>
				<?php /*?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>*/?>
			</article>
			
			<article id="menu_daimanzoku" class="menu_container photo-column-2 table-column-1 ico-2">
				<?php
					$page_id = get_page_by_path( 'viking/daimanzoku' );
					$page = get_post( $page_id );
										
					echo do_shortcode( $page->post_content );
				?>
				<?php /*?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>*/?>
			</article>
			
<?php /*?>
			<article id="menu_steak" class="menu_container photo-column-2 table-column-1 ico-2">
				<?php
					$page_id = get_page_by_path( 'steak' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
			</article>
*/?>
			
			<article id="menu_takeout" class="menu_container photo-column-2">
				<?php
					$page_id = get_page_by_path( 'takeout' );
					$page = get_post( $page_id );
										
					echo do_shortcode( $page->post_content );
				?>
				<?php /*?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>*/?>
			</article>
<?php /*?>
			
			<article id="menu_cafe" class="menu_container photo-column-2 table-column-1">
				<?php
					$page_id = get_page_by_path( 'cafe' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
			</article>
			
*/?>
			<article id="menu_party" class="menu_container photo-column-1 mb0">
				<?php
					$page_id = get_page_by_path( 'party' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<?php /*?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>*/?>
			</article>
			<?php /*
			<article id="menu_kids" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'kids' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_drink" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'drink' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_appetizer" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'appetizer' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_salad" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'salad' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_soup" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'soup' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_meat" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'meat' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_seafood" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'seafood' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_pasta" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'pasta' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_pizza" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'pizza' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_rice" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'rice' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_dessert" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'dessert' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_course" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'course' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_reservation" class="menu_box">
				<?php
					$page_id = get_page_by_path( 'reservation' );
					$page = get_post( $page_id );
					
					echo do_shortcode( $page->post_content );
				?>
				<p class="page_top font_en">
					<a href="#page">Page Top</a>
				</p>
			</article>
			
			<article id="menu_party" class="menu_box">
				<h1>Party<span>パーティー料理</span></h1>
	
				<div class="menu_inner clear">
					<p class="font_mincyo">ご予算に応じてご相談承ります。まずはお問い合わせください。</p>
					<span class="tel-link">
						<img src="<?php echo get_template_directory_uri(); ?>/images/menu/bnr_tel.png" alt="0825032401" />
					</span>
					<p class="page_top font_en">
						<a href="#page">Page Top</a>
					</p>
				</div>
			</article> */?>
			
			<?php //} ?>
		</div>
		
	</div>

<?php get_footer(); ?>