function initialize() {
  var latlng = new google.maps.LatLng(34.359781, 132.415220);
  var myOptions = {
    zoom: 17,
    center: latlng,
    scrollwheel: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    //disableDefaultUI: true,
    //draggable: false
  };
  var map = new google.maps.Map(document.getElementById('access_map'), myOptions);
 
var icon = new google.maps.MarkerImage('/wp-content/themes/pachipachi/images/access/icon_map.png',
    new google.maps.Size(60,70),
    new google.maps.Point(0,0)
  );
  var markerOptions = {
    position: latlng,
    map: map,
    icon: icon,
    title: 'seafood & style PachiPachi'
  };
  var marker = new google.maps.Marker(markerOptions);

  var styleOptions = [
     /*{
	  featureType: 'all',
	  elementType: 'labels',
	  stylers: [{ visibility: 'off' }]
	}, */{
	  featureType: 'all',
	  elementType: 'geometry',
	  stylers: [{ hue: '#73d6e0' }, { saturation: '-70' }, { gamma: '0.5' }]
	}];
  
  var styledMapOptions = { name: 'PachiPachi' }
  var mymapType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
  map.mapTypes.set('PachiPachi', mymapType);
  map.setMapTypeId('PachiPachi');
}