/*
	Fader v0.1.0
	Copyright (c) 2015 Suzuki
*/
(function($){
	$.fn.fader = function(options){
		if( this.length == 0 ) return this;
		if( this.length > 1 ){
			this.each(function(){
				$(this).fader(options);
			});
			return this;
		}

		var settings = $.extend({
			"start": 0,// 開始スライド
			"slides": "",// スライド要素
			"navs": null,// ナビゲーションボタン
			"prev": null,// 戻るボタン
			"next": null,// 進むボタン
			"speed": 320,// アニメーションのスピード
			"interval": 6400// 自動再生の間隔
		}, options);

		var elm = this;
		var current = settings.start;
		var slides = this.find(settings.slides).wrapAll("<div></div>");
		var navs = ( settings.navs == null ) ? null : this.find(settings.navs);
		var prev = ( settings.prev == null ) ? null : this.find(settings.prev);
		var next = ( settings.next == null ) ? null : this.find(settings.next);
		var board = slides.eq(0).parent();
		var lock = {
			"nav": false,
			"animate": false
		};

		if( this.css("position") == "static" ){ this.css("position", "relative");}
		slides.css({"position": "absolute", "top": "0", "left": "0"}).removeClass("current");;
		var slide_width = slides.eq(0).innerWidth();
		board.css({"position": "absolute", "left": "50%", "margin-left": slide_width / -2 + "px"});

		var animate = function(destination){
			if( destination == current ) return false;
			lock.animate = true;
			slides.eq(destination).clone(true).appendTo(board).fadeTo(0, 0).stop(true, false).fadeTo(settings.speed, 1, function(){
				lock.animate = false;
				set(destination);
			});
		}

		var set = function(index){
			board.empty();
			slides.eq(index).clone(true).appendTo(board).addClass("current");
			if( navs !== null ){ navs.removeClass("current").eq(index).addClass("current");}
			current = index;
		}

		var get_index = function(index){
			var result = index;
			while( result < 0 || result > slides.length - 1 ){
				result = result - (result / Math.abs(result) * slides.length);
			}
			return result;
		}

		set(current);

		if( navs !== null ){
			for(var i = 0; i < slides.length; i++){
				(function(i){
					navs.eq(i).on("click", function(){
						if( lock.animate ) return false;
						animate(i);
						lock.nav = true;
					});
				})(i);
			}
		}

		if( prev !== null && next !== null ){
			prev.on("click", function(){
				if( lock.animate ) return false;
				animate(get_index(current - 1));
				lock.nav = true;
			});

			next.on("click", function(){
				if( lock.animate ) return false;
				animate(get_index(current + 1));
				lock.nav = true;
			});
		}

		if( settings.interval ){
			setInterval(function(){
				if( !lock.nav && !lock.animate ){
					animate(get_index(current + 1));
				}
				lock.nav = false;
			}, settings.interval);
		}

		return this;
	}
})(jQuery);
