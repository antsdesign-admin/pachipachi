jQuery( document ).ready( function( $ ) {
	
    $('a[href^=#]').click(function() {
        var speed = 600;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
      });	
	
	// スマホ・タップで電話
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 && ua.indexOf('SC-01C') == -1 && ua.indexOf('A1_07') == -1 ){
        $('.tel-link img').each(function(){
            var alt = $(this).attr('alt');
            $(this).wrap($('<a>').attr('href', 'tel:' + alt.replace(/-/g, '')));
        });
    }   
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 && ua.indexOf('SC-01C') == -1 && ua.indexOf('A1_07') == -1 ){
        $('.tellink').each(function(){
            var str = $(this).text();
            $(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        });
    }
    
	$('#footer_slide').flexslider({
		controlNav : false,
		directionNav : true ,
		animation: "slide",
		slideshow: false,
		animationLoop: true,
		itemWidth: 316,
		itemMargin: 20,
		maxItems: 3,
		move: 1
	});
	
});

// ヘッダ,スマホフッタ固定
$(document).ready(function () {
    'use strict';
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 90) {
				/** 画面の上部がグローバルナビを越えたらクラスfixedをつける **/
				$('header,#footer_float').addClass("fixed");
				$('.fMenu').addClass("fixed");
			} else {
				/** 画面の上がグローバルナビを越えていなければクラスfixedをつけない **/
				$('header,#footer_float').removeClass("fixed");
				$('.fMenu').removeClass("fixed");
			}
		});
	});
});

// グローバルナビゲーション
$(function(){
    'use strict';
	$('#nav_toggle').click(function(){
		$('header').toggleClass('open');
		$('.grobalNavSP').slideToggle(500);
	});
});

// ページトップ固定
$(function() {
    'use strict';
    var topBtn = $('#pagetop');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

// トップ メニューバナー 高さ取得
//$(function(){
//    'use strict';
//    var biggestHeight = "0";
//    $("#front_bnr li .content *").each(function(){
//        if ($(this).height() > biggestHeight ) {
//            biggestHeight = $(this).height();
//        }
//    });
//    $("#front_bnr li .content").height(biggestHeight);
//});
