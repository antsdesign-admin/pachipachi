// JavaScript Document

  /**
   * ScrollMagic
   */
(function($){
	$(document).ready(function() {
		var controller = new ScrollMagic.Controller();
		$('.fadein').each(function() {
		var sceneFade = new ScrollMagic.Scene({
		  triggerElement: $(this).get(0),
		  triggerHook: 'onEnter',
		  offset: 400,
		  reverse: false
		});
		sceneFade.setClassToggle($(this).get(0), 'is-animated').addTo(controller);
		});
	});
})(jQuery);