<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP|Vollkorn:600i" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet" type="text/css">

	<?php
		wp_deregister_script('jquery');
	    wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', false, null);
	wp_head(); ?>
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/flexslider/flexslider.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/scroll-magic.css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/flexslider/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/script.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>

	<?php if ( is_front_page() ){ ?>
		<script src="<?php echo get_template_directory_uri(); ?>/js/script-front.js" type="text/javascript"></script>
	<?php } elseif ( get_post_type() == 'post' ) { ?>
		<script src="<?php echo get_template_directory_uri(); ?>/js/imgcentering.min.js" type="text/javascript"></script>
		<?php /*?><script src="<?php echo get_template_directory_uri(); ?>/js/script-event.js" type="text/javascript"></script> */?>
	<?php } if ( is_page( 'space' ) ) { ?>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/animate.css">
		<script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/script-space.js" type="text/javascript"></script>
	<?php } if ( is_page( 'access' ) ) { ?>
		<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/map.js" type="text/javascript"></script>
	<?php } ?>

<?php /*?>
<!--フッタの波-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.waterwave.js"></script>
<script type='text/javascript'>//<![CDATA[
$(function(){
    var box = $(".waterwave-front").waterwave({
//        direction: 'down',
        background: "wp-content/themes/pachipachi/images/footer/wave02.png"
//        color: '#00A6B6'
    });
});//]]>
$(function(){
    var box = $(".waterwave-back").waterwave2({
//        direction: 'down',
//        background: '<?php echo get_template_directory_uri(); ?>/images/footer/wave02.png'
//        color: '#00A6B6'
    });
});//]]>
</script>
*/?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-69207338-1', 'auto');
    ga('send', 'pageview');
</script>
</head>

<?php if  ( is_page( 'access' )) { ?>
<body onload="initialize();">
<?php } else { ?>
<body>
<?php }?>
<div id="page">
	<header class="pc">
		<div id="header-inner">
		<div id="nav-wrapper">
		<nav>
			<div id="main_navigation" class="wrapper">
				<a id="site_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/images/header/logo.png" alt="PachiPachiロゴ" />
				</a>
				<ul class="clear">
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">ホーム</a>
					</li>
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/">メニュー</a>
					</li>
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>event/">トピックス</a>
					</li>
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>staff/">スタッフ</a>
					</li>
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>access/">アクセス</a>
					</li>
				</ul>
			</div>
			<div id="sns-wrapper">
				<?php /* <a href="" target="_blank"><i class="fa fa-twitter-square"></i></a> */?>
				<a href="https://www.facebook.com/seasidestylepachipachi" target="_blank"><i class="fa fa-facebook-square"></i></a>
			</div>
			<div id="header_tel">
				<a href="#footer_tel"><i class="fa fa-phone"></i><span class="tellink">予約する</span></a>
			</div>
		</nav>
		</div>
		</div>
	</header>
	<header class="sp">
		<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/header/logo.png" alt="PachiPachi"></a></div>
		<div id="nav_toggle">
			<div> <span></span> <span></span> <span></span> </div>
		</div>
		<nav>
			<ul class="grobalNavSP">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">ホーム</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/">メニュー</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>event/">トピックス</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>staff/">スタッフ</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>access/">アクセス</a></li>
			</ul>
		</nav>
	</header>
	<div id="main">