<?php 
	$cat = get_the_category();
	$cat = $cat[0];
	$cat_name = $cat->name;
	$cat_slug = $cat->slug;
?>
<article class="event_content">

	<div class="entry_header">
		<?php /* if ( has_post_thumbnail()) { ?>
			<div class="entry_thumb img_container">
				<?php the_post_thumbnail( 'large' ); ?>
			</div>
		<?php } */?>
		
		<div class="entry_inner">
			<p class="posted_date"><?php echo get_post_time('Y.m.d D'); ?><?php /* echo $cat_name; */?></p>
			<?php if ( is_single() ){ ?>
				<h2 class="posted_title"><?php the_title(); ?></h2>
			<?php } else { ?>
				<h2 class="posted_title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
			<?php } ?>
		</div>
	</div>
	
	<div class="entry_content entry_inner">
		<?php the_content(); ?>
	<?php /*
		<p class="posted_category">カテゴリー：<?php echo $cat_name; ?> | Post：<?php echo get_post_time('Y.m.d l'); ?><?php the_time('G:i'); ?></p>
	*/ ?>
	</div>

	<footer class="entry_footer">
		<nav class="nav-single">
			<span class="nav_arc"><a href="<?php echo esc_url( home_url( '/' ) ); ?>event/">一覧へ</a></span>
		</nav>
		<ul id="entry_sns">
			<?php
			  $url_encode=urlencode(get_permalink());
			  $title_encode=urlencode(get_the_title());
			?>
<?php /*
			<li class="twitter"> 
				<a href="http://twitter.com/intent/tweet?url=<?php echo $url_encode ?>&text=<?php echo $title_encode ?>&via=【●ツイッターアカウント名（＠なし）●】&tw_p=tweetbutton">
					<i class="fa fa-twitter-square"></i><?php if(function_exists('scc_get_share_twitter')) echo (scc_get_share_twitter()==0)?'':scc_get_share_twitter(); ?>
				</a>
			</li>
*/ ?>
			<li class="facebook">
				<a href="http://www.facebook.com/sharer.php?src=bm&u=<?php echo $url_encode;?>&t=<?php echo $title_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;">
					<i class="fa fa-facebook-square"></i>
					<?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?>
				</a>
			</li>
<?php /*
			<li class="googleplus">
				<a href="https://plus.google.com/share?url=<?php echo $url_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;">
					<i class="fa fa-google-plus-square"></i><?php if(function_exists('scc_get_share_gplus')) echo (scc_get_share_gplus()==0)?'':scc_get_share_gplus(); ?>
				</a>
			</li>
*/ ?>
		</ul>
	</footer>
</article>
