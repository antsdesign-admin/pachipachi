<?php get_header(); ?>

	<div id="main_slide" class="flexslider">
		<ul class="slides">
<!--			<li>
				<div class="slide_inner">
					<div class="content_wrapper">
						<div class="content caption1 imgtxt">ステーキ食べ放題、<br>始まる。</div>
					</div>
					<div class="img img01">
						<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo10_sp.png" alt="" />
					</div>
				</div>
			</li>-->
<!-- テキストコーディング
			<li>
				<div class="slide_inner">
					<div class="content_wrapper">
						<div class="content caption1 imgtxt sp">ステーキ食べ放題、<br>始まる。</div>
					</div>
					<div class="img img01">
						<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo10.png" alt="" class="pc"/>
						<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo10_sp.png" alt="" class="sp"/>
					</div>
				</div>
			</li>
			<li>
				<div class="slide_inner">
					<div class="content_wrapper">
						<div class="content caption1">夏の新メニューが揃いました</div>
					</div>
					<div class="img">
						<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo11.png" alt=""/>
					</div>
				</div>
			</li>
-->
<?php /*
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo10.png" alt="" class="pc"/>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo10_sp.png" alt="" class="sp"/>
			</li>
*/?>
<?php /*
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo12.png" alt="" class="pc"/>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo12_sp.png" alt="" alt="" class="sp"/>
			</li>
*/?>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo13.png" alt="" alt="" class="pc"/>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo13_sp.png" alt="" class="sp"/>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo14.png" alt="" alt="" class="pc"/>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo14_sp.png" alt="" class="sp"/>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo15.png" alt="" alt="" class="pc"/>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo15_sp.png" alt="" class="sp"/>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo16.png" alt="" alt="" class="pc"/>
				<img src="<?php echo get_template_directory_uri(); ?>/images/main_slide/photo16_sp.png" alt="" class="sp"/>
			</li>
		</ul>
		<?php /*?>
		<img id="slide_logo" src="<?php echo get_template_directory_uri(); ?>/images/main_slide/logo.png" alt="PachiPachiロゴ" />
		<div id="top_border"></div>*/?>
	</div>

	<div id="front_topics" class="fadein">
		<h2><span class="font_en">Topics</span></h2>
		<div id="topics_inner">
			<?php
			$the_query = new WP_Query( 'posts_per_page=2' );
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) :
				$the_query->the_post();
				$cat = get_the_category();
				$cat = $cat[0];
				$cat_name = $cat->name;
				//newアイコン出力
				$days = 7; //何日間newをつけるか
				$today = date_i18n('U');
				$entry_day = get_the_time('U');
				$keika = date('U',($today - $entry_day)) / 86400;
			?>
				<a href="<?php the_permalink(); ?>">
					<div class="topics_box">
						<div class="img_box">
							<?php if ( has_post_thumbnail()) { ?>
							<div class="img" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);"></div>
								<?php } else { ?>
							<div class="img" style="background-image: url(<?php echo get_theme_file_uri( 'images/front/no_image.png' ); ?>);"></div>
								<?php } ?>
						</div>
						<div class="topics_content_box">
							<div class="topics_icon">
								<?php if ( is_sticky()): ?>
								<p class="font_en recommend">Recommend!</p>
								<?php endif ?>
								<?php if ( $days > $keika ): ?>
									<p class="font_en new">New!</p>
								<?php endif ?>
							</div>
							<div class="topics_date">
								<p><?php echo get_post_time('Y.m.d D'); ?></p>
							</div>
							<div class="topics_content">
								<h3><?php the_title(); ?></h3>
							</div>
						</div>
					</div>
				</a>
			<?php endwhile;
			endif;
			wp_reset_postdata();
			?>

<!--
			<?php global $post;
				$front_list = get_posts( array(
					'posts_per_page' => 3,
				));
				foreach( $front_list as $post ):
				setup_postdata( $post );

				$cat = get_the_category();
				$cat = $cat[0];
				$cat_name = $cat->name;
				//newアイコン出力
				$days = 7; //いつまでnewをつけるか
				$today = date_i18n('U');
				$entry_day = get_the_time('U');
				$keika = date('U',($today - $entry_day)) / 86400;
			?>
				<a href="<?php the_permalink(); ?>">
					<div class="topics_box">
						<div class="img_box">
							<div class="img">
								<?php if ( has_post_thumbnail()) { ?>
									<?php the_post_thumbnail( 'full' ); ?>
								<?php } else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/front/no_image.png" alt="">
								<?php } ?>
							</div>
						</div>
						<div class="topics_content_box">
							<div class="topics_icon">
								<?php if ( is_sticky()): ?>
								<p class="font_en recommend">Recommend!
								</p>
								<?php endif ?>
								<p class="font_en new">
								<?php if ( $days > $keika ):
									echo 'New!';
									endif;
								?>
								</p>
							</div>
							<div class="topics_date">
								<p><?php echo get_post_time('Y.m.d D'); ?></p>
							</div>
							<div class="topics_content">
								<h3><?php the_title(); ?></h3>
							</div>
						</div>
					</div>
				</a>
				<?php
				endforeach;
				wp_reset_postdata();
			?>
			-->
		</div>
		<div class="link">
			<a id="" class="btn btn_ichiran" href="<?php echo esc_url( home_url( '/' ) ); ?>event/">一覧をみる</a>
		</div>
	</div>

	<ul id="front_bnr" class="wrapper fadein">
	<?php /*?>
		<li>
			<a class="hover_fade" href="<?php echo esc_url( home_url( '/' ) ); ?>menu/">
				<img src="<?php echo get_template_directory_uri(); ?>/images/front/bnr_lunch.png" alt="LUNCH"/>
				<img class="img_on" src="<?php echo get_template_directory_uri(); ?>/images/front/bnr_lunch_on.png" alt="LUNCH"/>
			</a>
			<h2 class="font_mincyo">優雅なランチタイムを演出</h2>
			<p>地産池消をテーマとし、料理長が腕をふるった料理の数々に舌鼓。キラキラと光る海を眺めながら、ゆったりとしたひとときをお過ごしください。</p>
			　<img id="lunch_icon" class="menu_icon" src="<?php echo get_template_directory_uri(); ?>/images/front/icon_lunch.png" alt="LUNCH"/>
		</li>
		<li>
			<a class="hover_fade" href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_dinner">
				<img src="<?php echo get_template_directory_uri(); ?>/images/front/bnr_dinner.png" alt="DINNER"/>
				<img class="img_on" src="<?php echo get_template_directory_uri(); ?>/images/front/bnr_dinner_on.png" alt="DINNER"/>
			</a>
			<h2 class="font_mincyo">とろけるローストビーフが仲間入り</h2>
			<p>平日のディナータイム限定で、シェフ自らが切り分けるローストビーフは、今までの概念が覆るほどしっとりジューシー。</p>
			<img id="dinner_icon" class="menu_icon" src="<?php echo get_template_directory_uri(); ?>/images/front/icon_dinner.png" alt="DINNER"/>
		</li>
		<li class="last">
			<a class="hover_fade" href="<?php echo esc_url( home_url( '/' ) ); ?>space/">
				<img src="<?php echo get_template_directory_uri(); ?>/images/front/bnr_space.png" alt="SPACE"/>
				<img class="img_on" src="<?php echo get_template_directory_uri(); ?>/images/front/bnr_space_on.png" alt="SPACE"/>
			</a>
			<h2 class="font_mincyo">居心地のよい美食空間</h2>
			<p>身体にいい建材を使って建てたこだわりの空間。一足踏み入れたときから空気の違いを感じて頂けます。パーティーなど貸切予約も大歓迎です。</p>
			<img id="space_icon" class="menu_icon" src="<?php echo get_template_directory_uri(); ?>/images/front/icon_space.png" alt="SPACE"/>
		</li>*/?>
		<li class="fadein">
			<div class="img_box">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_viking" class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_menu01.png" alt="All-you-can-eat lunch and dinner"/></a>
			</div>
			<div class="content">
				<div class="content_inner">
					<h2><span class="ttl">バイキング</span><span class="font_en">All-you-can-eat lunch and dinner</span></h2>
					<p class="sub">組み合わせで料理の数は無限大！<br>みんなが満足できる食事を選ぶならバイキング！！</p>
					<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/" class="btn btn_more"><span class="font_en">Read more</span></a></div>
				</div>
			</div>
		</li>
<?php /*
		<li class="fadein">
			<div class="img_box">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_steak" class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_menu02.png" alt="All-you-can-eat steak"/></a>
			</div>
			<div class="content">
				<div class="content_inner">
					<h2><span class="ttl">ステーキ食べ放題</span><span class="font_en">All-you-can-eat steak</span></h2>
					<p class="sub">通常バイキングに追加料金で<br>ステーキが食べ放題に！</p>
					<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_dinner" class="btn btn_more"><span class="font_en">Read more</span></a></div>
				</div>
			</div>
		</li>
*/?>
<?php /*
		<li class="fadein">
				<div class="img_box">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_cafe" class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_menu03.png" alt="Cafe menu"/></a>
				</div>
				<div class="content">
						<div class="content_inner">
								<h2><span class="ttl">カフェ</span><span class="font_en">Cafe menu</span></h2>
								<p class="sub">美しい景色や海を眺めながら<br>ドリンクと手作りスイーツと共に。</p>
								<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#dinner_cafe" class="btn btn_more"><span class="font_en">Read more</span></a></div>
						</div>
				</div>
		</li>
*/?>
		<li class="fadein">
			<div class="img_box">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_takeout" class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_menu_takeout.png" alt="Takeout"/></a>
			</div>
			<div class="content">
				<div class="content_inner">
					<h2><span class="ttl">テイクアウト</span><span class="font_en">TAKEOUT</span></h2>
					<p class="sub">お家でPachiPachiの<br>味をお楽しみいただけます。</p>
					<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_takeout" class="btn btn_more"><span class="font_en">Read more</span></a></div>
				</div>
			</div>
		</li>
	</ul>

	<div id="front_link" class="fadein">
		<div class="photo-wrapper">
			<div class="img top fadein">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/#menu_party">
					<div class="img-inner pc"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_bnr01.png" alt="貸切パーティ"></div>
					<div class="img-inner sp"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_bnr_sp01.png" alt="貸切パーティ"></div>
					<div class="icon sp"><img src="<?php echo get_template_directory_uri(); ?>/images/front/ico_front_sp01.png" alt="貸切パーティ"></div>
				</a>
			</div>
			<div class="img left fadein">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>staff/">
					<div class="img-inner pc"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_bnr02.png" alt="スタッフ紹介"></div>
					<div class="img-inner sp"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_bnr_sp02.png" alt="スタッフ紹介"></div>
					<div class="icon sp"><img src="<?php echo get_template_directory_uri(); ?>/images/front/ico_front_sp02.png" alt="スタッフ紹介"></div>
				</a>
			</div>
			<div class="img right fadein">
				<a href="https://my.matterport.com/show/?m=QhHV18AXLkW&fbclid=IwAR3LSwDca6JSnmwPFiGmxYe0Dn2ikvNJtisIUItXtpArR5joCqts9ynXjms" target="_blank">
					<div class="img-inner pc"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_bnr03.png" alt="PachiPachi VR"></div>
					<div class="img-inner sp"><img src="<?php echo get_template_directory_uri(); ?>/images/front/front_bnr_sp03.png" alt="PachiPachi VR"></div>
					<div class="icon sp"><img src="<?php echo get_template_directory_uri(); ?>/images/front/ico_front_sp03.png" alt="PachiPachi VR"></div>
				</a>
			</div>
		</div>
	</div>

	<div id="front_insta" class="fadein">
		<div class="logo"><a href="https://www.instagram.com/pachipachi.since2015.6.12/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/front/logo_insta.png" alt="インスタグラム"></a>
		</div>
<?php /*
		<div class="ttl">
		新メニューやイベントの情報などを発信中!
		</div>
*/?>
<?php /*
		<div class="widget">
<!-- SnapWidget -->
<!--<iframe src="https://snapwidget.com/embed/696056" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:1918px; height:274px"></iframe>-->
			<div class="column-7">
			<!-- InstaWidget -->
			<a href="https://instawidget.net/v/user/pachipachi.since2015.6.12" id="link-736522159c0d7c2dd3ed6e17d59d2dd9340cddd5e2feb9a4a5b7f968f7fdc8c7">@pachipachi.since2015.6.12</a>
			<script src="https://instawidget.net/js/instawidget.js?u=736522159c0d7c2dd3ed6e17d59d2dd9340cddd5e2feb9a4a5b7f968f7fdc8c7&width=100%"></script>
			</div>
			<div class="column-4">
			<!-- InstaWidget -->
			<a href="https://instawidget.net/v/user/pachipachi.since2015.6.12" id="link-537622ec7481346d0cda47fba7a5eaaaac8d06445170085fa66d3d83e7cf0f8c">@pachipachi.since2015.6.12</a>
			<script src="https://instawidget.net/js/instawidget.js?u=537622ec7481346d0cda47fba7a5eaaaac8d06445170085fa66d3d83e7cf0f8c&width=100%"></script>
			</div>
			<div class="column-2">
			<!-- InstaWidget -->
			<a href="https://instawidget.net/v/user/pachipachi.since2015.6.12" id="link-aaa0a76cc5b2618467ef680f840d4f1ac153ab86aacf0626f9043147928a1b65">@pachipachi.since2015.6.12</a>
			<script src="https://instawidget.net/js/instawidget.js?u=aaa0a76cc5b2618467ef680f840d4f1ac153ab86aacf0626f9043147928a1b65&width=100%"></script>
			</div>
		</div>
*/?>
	</div>

	<div id="front_etc" class="fadein">
		<div class="etc_box">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>access">
				<div class="etc_img fadein">
					<img src="<?php echo get_template_directory_uri(); ?>/images/front/front_etc01.png" alt="アクセス">
				</div>
				<div class="etc_content fadein">
					<img src="<?php echo get_template_directory_uri(); ?>/images/front/ico_access.png" alt="">
					<div class="sub">アクセス</div>
				</div>
			</a>
		</div>
	</div>

	<?php /*?>
	<div class="wrapper">
		<div id="front_access">
			<h2 class="font_en"><i class="fa fa-car"></i>Access</h2>
			<div id="access_inner">
				<h3 class="font_en">PachiPachi<span>SEA SIDE & STYLE</span></h3>
				<p>広島県広島市西区観音新町4-14<br />
					営業時間 ランチ11:00〜15:00<br />
					　　　　 ディナー17:00〜21:00<br />
					定休日 不定休  ／  駐車場　全日無料開放<br />
					<span class="font_en tellink"><i class="fa fa-phone"></i>082-503-2401</span>
					<a id="access_more" class="replace" href="<?php echo esc_url( home_url( '/' ) ); ?>access/">詳しくみる</a>
				</p>
			</div>
		</div>
	</div>
	
	<div id="front_bottomImg">
		<img src="<?php echo get_template_directory_uri(); ?>/images/front/bottom_photo.png" alt="PHOTO" />
	</div>
	
	*/?>

<?php get_footer(); ?>