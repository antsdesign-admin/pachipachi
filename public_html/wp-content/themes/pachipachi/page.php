<?php get_header(); ?>
	
	<?php 
		while ( have_posts() ) : the_post();
		
			remove_filter('the_content', 'wpautop');
			the_content(); 
			
		endwhile;
	?>

<?php get_footer(); ?>