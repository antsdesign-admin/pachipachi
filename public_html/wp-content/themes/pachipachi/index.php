<?php get_header(); ?>
	<div id="event_title" class="header_title">
		<h1><span class="font_en">Topics</span>トピックス</h1>
	</div>
	<div id="page_topics">
		<div class="topics-wrapper">
			<div class="clear">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				<?php if(function_exists('bcn_display'))
				{
					bcn_display();
				}?>
			</div>
			</div>
			<ul class="topics-box fadein">
			<?php if(have_posts()): while (have_posts()) : the_post();?>
				<li>
					<a href="<?php the_permalink();?>">
						<div class="img_box">
							<?php if ( has_post_thumbnail()) { ?>
							<div class="img" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);"></div>
								<?php } else { ?>
							<div class="img" style="background-image: url(<?php echo get_theme_file_uri( 'images/front/no_image.png' ); ?>);"></div>
								<?php } ?>
<!--
							<div class="img">
								<?php if ( has_post_thumbnail() ) { the_post_thumbnail('large'); }else{?><img src="<?php echo get_template_directory_uri(); ?>/images/front/no_image.png" alt=""><?php } ?>
							</div>
-->
						</div>
						<div class="topics_content_box">
							<div class="date"><?php echo get_post_time('Y.m.d D'); ?></div>
							<div class="content"><?php the_title(); ?></div>
						</div>
					</a>
				</li>
			<?php endwhile; endif;?>
			</ul>
			
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(
				array(
					'options' => array( 
						'prev_text' => " ",
						'next_text' => " ",
					),
				)
			); }?>
		</div>
		<!--
		<div class="wrapper">
			<?php /* get_sidebar(); ?>
			<div id="main_container">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content' ); ?>

				<?php endwhile; ?>

				<?php if(function_exists('wp_pagenavi')) : wp_pagenavi(); endif; */?>
			</div>

		</div>
		-->
	</div><!--#page_topics-->
<?php get_footer(); ?>