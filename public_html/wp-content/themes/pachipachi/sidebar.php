<div id="side_container">
	<div class="side_box">
		<h2 class="font_en"><i class="fa fa-flag"></i>New</h2>
		<ul class="blog_list">
			<?php global $post;
		 		$blog_list = get_posts( array(
			   		'post_type' => 'post',
			   		'posts_per_page' => '5',				 				 
			   ));
			   foreach( $blog_list as $post ):
			   setup_postdata( $post );
			?>
			<li><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></li>
			<?php
		  		endforeach;
				wp_reset_postdata();
			?>
		</ul>
	</div>
	
	<div class="side_box">
		<h2 class="font_en">Category</h2>
		<ul><?php wp_list_categories( 'orderby=ID&show_count=1&hide_empty=1&hierarchical=1&title_li=' ); ?> </ul>
	</div>
	
	<div class="side_box">	
		<h2 class="font_en">Archive</h2>
		<ul>
			<select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'> 
				<option value=""><?php echo esc_attr('- 月を選択してください -'); ?></option> 
				<?php wp_get_archives('type=monthly&format=option&show_post_count=1'); ?>
			</select>
		</ul>
	</div>
	
	<div class="side_box">
		<h2 class="font_en">Search</h2>
		<?php get_search_form(); ?>
	</div>
</div>