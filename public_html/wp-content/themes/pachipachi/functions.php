<?php
if ( ! isset( $content_width ) )
	$content_width = 710;

function my_setup() {
	add_editor_style('css/editor-style.css');
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 710, 9999 ); 
}
add_action( 'after_setup_theme', 'my_setup' );


function my_scripts_styles() {
	global $wp_styles;
	wp_enqueue_style( 'my-normalize', get_template_directory_uri() . '/css/normalize.css' , array(), NULL );	
	wp_enqueue_style( 'my-style', get_stylesheet_uri() , array( 'my-normalize' ) );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_styles' );


//	WordPressの情報を削除
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


//固定ページの画像パス書き換え
function replaceImagePath($arg) {
	$content = str_replace( '"images/', '"' . get_stylesheet_directory_uri() . '/images/', $arg);
	return $content;
}
add_action('the_content', 'replaceImagePath');


// 管理画面固定ページのビジュアルエディタ削除
add_filter('user_can_richedit' , 'my_default_editor');

function my_default_editor( $r ) {
	if ( 'page'== get_current_screen()->id ) {
		return false;
	}
	return $r;
}


// 左メニュー名変更
function edit_admin_menus() {
	global $menu;
	global $submenu;

	$menu[5][0] = 'Topics';
	$submenu['edit.php'][5][0] = 'Topics一覧';

	$menu[20][0] = 'ページ編集';
	$submenu['edit.php?post_type=page'][5][0] = 'ページ一覧';

	// メディアの場所を21に移動
	$menu[21] = $menu[10];
	unset($menu[10]);

}
add_action('admin_menu', 'edit_admin_menus');



// 左メニュー削除
function remove_menu() {

	//「コメント」非表示
	remove_menu_page( 'edit-comments.php');

	// 投稿の「タグ」を非表示
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
}
add_action('admin_menu', 'remove_menu');


// 「BLOG」投稿画面から不要項目削除
function remove_post_metaboxes() {
	remove_meta_box( 'postcustom', 'post' , 'normal' ); // カスタムフィールド
	remove_meta_box( 'postexcerpt', 'post' , 'normal' ); // 抜粋
	remove_meta_box( 'slugdiv', 'post' , 'normal' ); // スラッグ設定
	remove_meta_box( 'authordiv', 'post' , 'normal' ); // 投稿者
	remove_meta_box( 'commentstatusdiv', 'post' , 'normal' ); // コメント設定
	remove_meta_box( 'tagsdiv-post_tag', 'post' , 'normal' ); // タグ
}
add_action('admin_menu', 'remove_post_metaboxes');


// 「BLOG」一覧不要項目削除（管理画面）
function custom_post_columns ($columns) {
	unset($columns['author']); // 作成者
	unset($columns['tags']); // タグ、カスタムフィールド
	unset($columns['comments']); // コメント
	return $columns;
}
add_filter('manage_posts_columns', 'custom_post_columns');


//　「編集ページ」一覧不要項目削除（管理画面）
function custom_page_columns ($columns) {
	unset($columns['author']); // 作成者
	unset($columns['tags']); // タグ、カスタムフィールド
	unset($columns['comments']); // コメント
	return $columns;
}
add_filter('manage_pages_columns', 'custom_page_columns');


// 画像のみpタグで囲わない
function remove_p_on_images($content){
	return preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
}
add_filter('the_content', 'remove_p_on_images');

function filter_ptags_on_link_images($content)
{
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_link_images');


// 記事並び替え設定
function my_pagesize( $query ) {
	if ( is_admin() || ! $query->is_main_query() )
		return;

	if ( is_search() ) {
		$query->set( 'post_type', 'post' );
		return;
	}
}
add_action( 'pre_get_posts', 'my_pagesize', 1 );

// パンくず 固定ページ用
function short_php($params = array()) {
extract(shortcode_atts(array(
'file' => 'default'
), $params));
ob_start();
include(STYLESHEETPATH . "/$file.php");
return ob_get_clean();
}
 
add_shortcode('myphp', 'short_php');

// メニュー画像
/**
 * shortcode
 */

add_shortcode('getImg',function($attr){
	ob_start();
	if( get_field('ff_img_left',$attr['id'])){
		$img01 = '<div class="img-inner fadein"><img src="'.get_field('ff_img_left',$attr['id']).'" alt=""></div>';
	}
	if( get_field('ff_img_right',$attr['id'])){
		$img02 = '<div class="img-inner fadein"><img src="'.get_field('ff_img_right',$attr['id']).'" alt=""></div>';
	}
	$html = '<div class="photo-wrapper fadein"><div class="img left">'.$img01.'</div><div class="img right">'.$img02.'</div></div>';
	return $html;
});