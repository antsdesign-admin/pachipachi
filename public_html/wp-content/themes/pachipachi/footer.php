	</div><?php  //#main ?>

	<footer id="site_footer">
		<?php /*?>
		<div id="footer_link">
			<h2><i class="fa fa-external-link"></i><span class="font_en">Link</span></h2>
			<div id="footer_slide" class="flexslider wrapper">
				<ul class="slides">
					<li>
						<a href="http://www.odawara.cc/" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/images/footer/bnr1.png" alt="株式会社 小田原ハウジング"/>
						</a>
					</li>
					<li>
						<a href="http://jyuigaku.com/" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/images/footer/bnr2.png" alt="住医学研究会"/>
						</a>
					</li>
				</ul>
			</div>
		</div>*/?>
		<div id="footer_bottom">
			<div class="wrapper">
				<div class="bg_img"><img src="<?php echo get_template_directory_uri(); ?>/images/footer/footer_bg.png" alt="PachiPachi"/></div>
				<a id="footer_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/images/footer/logo.png" alt="PachiPachiロゴ"/>
				</a>
				<div id="footer_contact_box">
					<div class="pc">
						<div id="footer_contact">ご予約・お問い合わせ</div>
						<div id="footer_tel" class="font_mincyo"><span class="tellink font_en">Tel 082-503-2401</span></div>
					</div>
					<div id="footer_company">株式会社リンク<br>〒737-0112広島県呉市広古新開9-9-19 <!--<a href=”tel:0823740260″>-->tel:0823-74-0260</a></div>
				</div>
					<nav id="footer_nav">
						<ul>
							<li>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">ホーム</a>
							</li>
							<li>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/">メニュー</a>
							</li>
							<li>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>event/">トピックス</a>
							</li>
							<li>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>staff/">スタッフ</a>
							</li>
							<li>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>access/">アクセス</a>
							</li>
						</ul>
					</nav>
					<div id="footer_copy" class="font_en">COPYRIGHT © 2015 PachiPachi SEA SIDE & STYLE.INC ALL RIGHTS RESERVED.</div>
				</div>
			</div>
			<div id="wave-content">
				<div class="wave-wrapper">
					<div class="wave">
					<img src="<?php echo get_template_directory_uri(); ?>/images/footer/wave_stop.png" alt=""/>
<!--					<img src="<?php echo get_template_directory_uri(); ?>/images/footer/wave_stop_sp.png" alt="" class="sp"/>-->
					</div>
				</div>
			</div>
			<?php /*?>
			<div id="canvas-cont">
				<canvas class="waterwave-canvas waterwave-front"></canvas>
				<canvas class="waterwave-canvas waterwave-back"></canvas>
			</div>
			*/?>
		</div>

		<div id="footer_float" class="sp">
			<div class="float_wrapper">
				<div class="sub">ご予約・お問い合わせ</div>
				<div class="tel"><a href="tel:0825032401" data-label="電話番号-フッタ共通"><span class="font_en">Tel 082-503-2401</span></a></div>
			</div>
		</div>

		<div id="footer_middle">
			<div id="pagetop">
				<a href="#page">PAGETOP</a>
				<?php /*?><a class="replace" href="#page">PAGETOP</a>*/?>
			</div>
		</div>

	</footer>
	
</div><!-- #page -->

<script src="<?php echo get_template_directory_uri(); ?>/js/scroll-magic.js" type="text/javascript"></script>
<script>
$('a[href^="tel:"]').on('click', function(e) {
	var self = $(this);
	var href = self.attr('href');
	var tel = href.replace('tel:', '');
	var label = self.attr('data-label');

	if (typeof gtag === 'function') {
		gtag(
			'event',
			'click',
			{
				'event_category': '電話番号',
				'event_label': label
			}
		);
	}
});
</script>

<?php wp_footer(); ?>
</body>
</html>