<?php get_header(); ?>

	<div id="event_title" class="header_title">
		<h1><span class="font_en">Topics</span>トピックス</h1>
	</div>

	<div id="page_topics">
		<div class="wrapper">
			<div id="main_container">
				<div class="clear">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
					<?php if(function_exists('bcn_display'))
					{
						bcn_display();
					}?>
				</div>
				</div>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content' ); ?>

<?php /*
					<nav class="nav-single clear">
						<span class="nav-previous"><?php next_post_link( '%link', '<span class="meta-nav">' . '＜' . '</span>' ); ?></span>
						<span class="nav_arc"><a href="<?php echo esc_url( home_url( '/' ) ); ?>event/">一覧へ</a></span>
						<span class="nav-next"><?php previous_post_link( '%link', '<span class="meta-nav">' . '＞'. '</span>' ); ?></span>
					</nav>
*/?>
				<?php endwhile; ?>

			</div>

		</div>
	</div><!--#page_topics-->

<?php get_footer(); ?>