<div class="sns_btn clear">
	<!-- twitter -->
	<span class="tw">
		<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
			<script>!function(d,s,id){
				var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
					if(!d.getElementById(id)){js=d.createElement(s);js.id=id;
					js.src=p+'://platform.twitter.com/widgets.js';
					fjs.parentNode.insertBefore(js,fjs);
					}
				}(document, 'script', 'twitter-wjs');
			</script>
	</span>
	<!-- twitter END -->
		
	<!-- facebook -->
	<span class="fb">
		<iframe src="//www.facebook.com/v2.0/plugins/like.php?href=<?php the_permalink(); ?>&amp;
			width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;
			share=false&amp;height=35&amp;" 
			scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true">
		</iframe>		
	</span>
	<!-- facebook END -->
	
	<!-- google plus -->
	<span class="google1">
		<div class="g-plusone" data-annotation="bubble" data-size="medium"></div>
			<script type="text/javascript">
			  window.___gcfg = {lang: 'ja'};
			
			  (function() {
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/platform.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
	</span>
	<!-- google plus END -->
		
</div>		

<?php /* for template = <?php get_template_part( 'social-buttonset' ); ?> */ ?>